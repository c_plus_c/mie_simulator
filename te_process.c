/*
 * te_process.c
 *
 *  Created on: 2014/11/28
 *      Author: cplusc
 */


#include "te_process.h"
#include "simulator.h"
#include "cylinder_scatterer.h"
#include <stdio.h>
#include <math.h>

//誘電率
static double *EpsHz=NULL;
static double *EpsEx=NULL;
static double *EpsEy=NULL;

//Hx(x,y)
static double complex *Ex=NULL;

//Hy(x,y)
static double complex *Ey=NULL;

//Ez(x,y)
static double complex *Hzx=NULL;
static double complex *Hzy=NULL;
static double complex *Hz=NULL;

static double *C_EX1=NULL;
static double *C_EX2=NULL;

static double *C_EY1=NULL;
static double *C_EY2=NULL;

static double *C_HZX1=NULL;
static double *C_HZX2=NULL;

static double *C_HZY1=NULL;
static double *C_HZY2=NULL;

static double gamma2;

static void addWave()
{
	double rad = waveAngle;
	double ray_coef = 1.0 - exp(-0.0001*time*time);
	//電場を印加

	double co = cos((waveAngle+90)*M_PI/180);
	double si = sin((waveAngle+90)*M_PI/180);
	for (int i = n_pml; i < n_px - n_pml; ++i)
	{
		for (int j = n_pml; j < n_py - n_pml; ++j)
		{
			double kr = 1.0*i*k*cos(rad) + 1.0*j*k*sin(rad);
			double n = sqrt( EpsEx[ind(i,j)] / EPSILON_0);
			double u0 = sin(omega*0.5) / sin(k*0.5);
			double u1 = sin(omega*0.5) / sin(k*n*0.5);
			double _n = u0 / u1;

			double complex w = ray_coef * (1.0/(_n*n) - 1.0)* (cexp(I*(kr-omega*(time+1))) - cexp(I*(kr-omega*(time-0))));
			Ex[ind(i,j)] += w*co;
		}
	}

	for (int i = n_pml; i < n_px - n_pml; ++i)
	{
		for (int j = n_pml; j < n_py - n_pml; ++j)
		{
			double kr = 1.0*i*k*cos(rad) + 1.0*j*k*sin(rad);
			double n = sqrt( EpsEy[ind(i,j)] / EPSILON_0);
			double u0 = sin(omega*0.5) / sin(k*0.5);
			double u1 = sin(omega*0.5) / sin(k*n*0.5);
			double _n = u0 / u1;

			double complex w = ray_coef * (1.0/(_n*n) - 1.0)* (cexp(I*(kr-omega*(time+1))) - cexp(I*(kr-omega*(time-0))));
			Ey[ind(i,j)] += w*si;
		}
	}

}

static double getUnsEx(int x,int y)
{
	double n = sqrt(EpsEx[ind(x,y)]/EPSILON_0);
	return sin(omega/2)/sin(n*k/2);
}

static double getUnsEy(double x,double y)
{
	double n = sqrt(EpsEy[ind(x,y)]/EPSILON_0);
	return sin(omega/2)/sin(n*k/2);
}

static double getUnsHz(double x,double y)
{
	double n = sqrt(EpsHz[ind(x,y)]/EPSILON_0);
	return sin(omega/2)/sin(n*k/2);
}

void te_init()
{
	printf("TE Init\n");

	Ex = (double complex*)malloc(sizeof(double complex)*n_cell);
	Ey = (double complex*)malloc(sizeof(double complex)*n_cell);
	Hzx = (double complex*)malloc(sizeof(double complex)*n_cell);
	Hzy = (double complex*)malloc(sizeof(double complex)*n_cell);
	Hz = (double complex*)malloc(sizeof(double complex)*n_cell);

	EpsEx = (double*)malloc(sizeof(double)*n_cell);
	EpsEy = (double*)malloc(sizeof(double)*n_cell);
	EpsHz = (double*)malloc(sizeof(double)*n_cell);

	C_EX1 = (double*)malloc(sizeof(double)*n_cell);
	C_EX2 = (double*)malloc(sizeof(double)*n_cell);

	C_EY1 = (double*)malloc(sizeof(double)*n_cell);
	C_EY2 = (double*)malloc(sizeof(double)*n_cell);

	C_HZX1 = (double*)malloc(sizeof(double)*n_cell);
	C_HZX2 = (double*)malloc(sizeof(double)*n_cell);

	C_HZY1 = (double*)malloc(sizeof(double)*n_cell);
	C_HZY2 = (double*)malloc(sizeof(double)*n_cell);


	//電磁界初期化
	for (int i = 0; i < n_px; ++i)
	{
		for (int j = 0; j < n_py; ++j)
		{
			Ex[ind(i,j)] = 0;
			Ey[ind(i,j)] = 0;
			Hzx[ind(i,j)] = 0;
			Hzy[ind(i,j)] = 0;
			Hz[ind(i,j)] = 0;

			C_EX1[ind(i,j)] = C_EY1[ind(i,j)] = C_HZX1[ind(i,j)] = C_HZX2[ind(i,j)] = 1;
			C_EX2[ind(i,j)] = C_EY2[ind(i,j)] = C_HZY1[ind(i,j)]= C_HZY2[ind(i,j)] = 1;
		}
	}

	//誘電率初期化
	for(int i=0;i<n_px;++i)
	{
		for(int j=0;j<n_py;++j)
		{
			EpsEx[ind(i,j)] = get_epsilon(i+0.5,j,1,1,n_px/2, n_py/2, lambda,0,1);
			EpsEy[ind(i,j)] = get_epsilon(i,j+0.5,1,1,n_px/2, n_py/2, lambda,1,0);
			EpsHz[ind(i,j)] = 0.5*(get_epsilon(i+0.5,j+0.5,1,1,n_px/2, n_py/2, lambda,1,0)+get_epsilon(i+0.5,j+0.5,1,1,n_px/2, n_py/2, lambda,0,1));
		}
	}

	for (int i = 0; i < n_px; ++i)
	{
		for (int j = 0; j < n_py; ++j)
		{
			double alpha_s = conductivityY(j+0.5,ELECTRIC)/(2*EpsEx[ind(i,j)]);
			double alpha = conductivityY(j+0.5,MAGNETIC)/(2*magnetic_permeability);
			double beta = tanh(alpha_s)/(1+tanh(alpha)*tanh(alpha_s));
			double betaplus = 1 + beta;
			double betaminus = 1 - beta;
			double udash = getUnsEx(i,j);
			double coef = udash*sqrt(magnetic_permeability/EpsEx[ind(i,j)]);
			C_EX1[ind(i,j)] = betaminus/betaplus;
			C_EX2[ind(i,j)] = coef / betaplus;
		}
	}

	for (int i = 0; i < n_px; ++i)
	{
		for (int j = 0; j < n_py; ++j)
		{
			double alpha_s = conductivityX(i+0.5,ELECTRIC)/(2*EpsEy[ind(i,j)]);
			double alpha = conductivityX(i+0.5,MAGNETIC)/(2*magnetic_permeability);
			double beta = tanh(alpha_s)/(1+tanh(alpha)*tanh(alpha_s));
			double betaplus = 1 + beta;
			double betaminus = 1 - beta;
			double udash = getUnsEy(i,j);
			double coef = udash*sqrt(magnetic_permeability/EpsEy[ind(i,j)]);
			C_EY1[ind(i,j)] = betaminus/betaplus;
			C_EY2[ind(i,j)] = coef / betaplus;
		}
	}

	//γ'の計算 式(3.4.26)
	{
		gamma2 = 1.0/12.0;// + k*k/180.0 - pow(k,4)/23040.0)/2.0;
	}

	for (int i = 0; i < n_px; ++i)
	{
		for (int j = 0; j < n_py; ++j)
		{
			double alpha_s = conductivityX(i+0.5,ELECTRIC)/(2*EpsHz[ind(i,j)]);
			double alpha = conductivityX(i+0.5,MAGNETIC)/(2*magnetic_permeability);
			double beta = tanh(alpha)/(1+tanh(alpha)*tanh(alpha_s));
			double betaplus = 1 + beta;
			double betaminus = 1 - beta;
			double udash = getUnsHz(i,j);
			double coef = udash * sqrt(EpsHz[ind(i,j)]/magnetic_permeability);
			C_HZX1[ind(i,j)] = betaminus/betaplus;
			C_HZX2[ind(i,j)] = coef / betaplus;
		}
	}

	for (int i = 0; i < n_px; ++i)
	{
		for (int j = 0; j < n_py; ++j)
		{
			double alpha_s = conductivityY(j+0.5,ELECTRIC)/(2*EpsHz[ind(i,j)]);
			double alpha = conductivityY(j+0.5,MAGNETIC)/(2*magnetic_permeability);
			double beta = tanh(alpha)/(1+tanh(alpha)*tanh(alpha_s));
			double betaplus = 1 + beta;
			double betaminus = 1 - beta;
			double udash = getUnsHz(i,j);
			double coef = udash * sqrt(EpsHz[ind(i,j)]/magnetic_permeability);
			C_HZY1[ind(i,j)] = betaminus/betaplus;
			C_HZY2[ind(i,j)] = coef/betaplus;
		}
	}
}

void te_update()
{

	/*
	hx[x][y]=Hx(x,y+1/2)
	hy[x][y]=Hy(x+1/2,y)
	ez[x][y]=Ez(x,y)
	として定義する。
	 */

	//Hzxの計算
	for (int i = 0; i < n_px-1; ++i)
	{
		for (int j = 0; j < n_py; ++j)
		{
			Hzx[ind(i,j)] = C_HZX1[ind(i,j)]*Hzx[ind(i,j)] - C_HZX2[ind(i,j)] *(Ey[ind(i+1,j)]-Ey[ind(i,j)]);// + C_EZX3[ind(i,j)] * JEx[ind(i,j)];
		}
	}

	//Hzyの計算
	for (int i = 0; i < n_px; ++i)
	{
		for (int j = 0; j < n_py-1; ++j)
		{
			Hzy[ind(i,j)] = C_HZY1[ind(i,j)]*Hzy[ind(i,j)] + C_HZY2[ind(i,j)] *(Ex[ind(i,j+1)]-Ex[ind(i,j)]);// + C_EZY3[ind(i,j)] * JEy[ind(i,j)];
		}
	}


	for (int i = 0; i < n_px; ++i)
	{
		for (int j = 0; j < n_py; ++j)
		{
			Hz[ind(i,j)] = Hzx[ind(i,j)]+Hzy[ind(i,j)];
		}
	}

	//Exの計算 式(3.4.31)
	for (int i = 1; i < n_px-1; ++i)
	{
		for (int j = 1; j < n_py; ++j)
		{
			double complex d1 = (Hz[ind(i,j)]-Hz[ind(i,j-1)]);
			double complex d2 = ((Hz[ind(i+1,j)]-Hz[ind(i+1,j-1)]) + (Hz[ind(i-1,j)]-Hz[ind(i-1,j-1)]) -2*(d1))*gamma2;
			double complex diff=d1+d2;

			Ex[ind(i,j)] = C_EX1[ind(i,j)]*Ex[ind(i,j)] + C_EX2[ind(i,j)] *diff;
		}
	}

	//Eyの計算 式(3.4.31)
	for (int i = 1; i < n_px; ++i)
	{
		for (int j = 1; j < n_py-1; ++j)
		{

			double complex d1 = (Hz[ind(i,j)]-Hz[ind(i-1,j)]);
			double complex d2 = ((Hz[ind(i,j+1)]-Hz[ind(i-1,j+1)]) + (Hz[ind(i,j-1)]-Hz[ind(i-1,j-1)]) -2*(d1))*gamma2;
			double complex diff=d1+d2;

			Ey[ind(i,j)] = C_EY1[ind(i,j)]*Ey[ind(i,j)] - C_EY2[ind(i,j)] *diff;
		}
	}

	addWave();


	if(time==2000)
	{
		printf("CSV Saved");
		FILE *fp = fopen("te_out.csv","w");
		for(int i=180;i>=0;--i)
		{
			double complex val = cbilinear(Ey, n_px/2.0 + lambda*1.2*cos(i*M_PI/180.0), n_py/2.0 + lambda*1.2*sin(i*M_PI/180.0), n_px, n_py);
			fprintf(fp,"%f\n",cabs(val));
		}

		fclose(fp);
	}
	//printf("%d\n",);
}


void te_setdraw()
{
	//描画データ更新


	for (int i = 0; i < n_px; ++i)
	{
		for (int j = 0; j < n_py; ++j)
		{
			double ezr = cabs(Ey[ind(i,j)]);

			double range = 1.0; //波の振幅
			double ab_phi = ezr < 0 ? -ezr : ezr;

			double a = ab_phi < range ? (ab_phi <  range/3.0 ? 3.0/range*ab_phi : (-3.0/4.0/range*ab_phi+1.25) ) : 0.5;

			double r = ezr > 0 ? a:0;
			double b = ezr < 0 ? a:0;
			double g = fmin(1.0, fmax(0.0, -3*ab_phi+2));

			draw_buffer[ind(i,j)*3]   = (GLubyte)( fmax(0, 255*(r - 0.5*(EpsEy[ind(i,j)] - EPSILON_0))));
			draw_buffer[ind(i,j)*3+2] = (GLubyte)( fmax(0, 255*(b - 0.5*(EpsEy[ind(i,j)] - EPSILON_0))));
			draw_buffer[ind(i,j)*3+1] = (GLubyte)( fmax(0, 255*(g - 0.5*(EpsEy[ind(i,j)] - EPSILON_0))));
		}
	}

	//printf("%f\n",Ez[ind(52,50)]);
	fflush(stdout);
}

void te_end()
{
	printf("TE End\n");
	free(Ex);
	free(Ey);
	free(Hz);
	free(Hzx);
	free(Hzy);

	free(EpsEx);
	free(EpsEy);
	free(EpsHz);

	free(C_EX1);
	free(C_EX2);

	free(C_EY1);
	free(C_EY2);

	free(C_HZX1);
	free(C_HZY1);
	free(C_HZX2);
	free(C_HZY2);
}
