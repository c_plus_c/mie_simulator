/*
 * te_process.h
 *
 *  Created on: 2014/11/28
 *      Author: cplusc
 */

#ifndef TE_PROCESS_H_
#define TE_PROCESS_H_

#include "externs.h"

void te_init();

void te_update();

void te_setdraw();

void te_end();

#endif /* TE_PROCESS_H_ */
