/*
 * cylinderScatterer.c
 *
 *  Created on: 2014/10/12
 *      Author: cplusc
 */
#include <math.h>
#include "cylinder_scatterer.h"

#define X_PRECISION 100.0
#define Y_PRECISION 100.0

double get_epsilon(double x, double y, double sizeX, double sizeY, double scatCenterX, double scatCenterY, double scatRadius,double col,double row)
{

	if(x < n_pml || y < n_pml || x > n_x+n_pml || y > n_y + n_pml)
		return EPSILON_0;

	double dx = x-scatCenterX;
	double dy = y-scatCenterY;

	double len = pow(dx,2)+pow(dy,2);

	if(len>=(scatRadius+1)*(scatRadius+1))
		return EPSILON_0;
	else if(len<=(scatRadius-1)*(scatRadius-1))
		return SCATTERER_EPSILON;

	double sum=0;
	for(double i=-X_PRECISION+0.5;i<X_PRECISION;i+=1)
	{
		for(double j=-Y_PRECISION+0.5;j<Y_PRECISION;j+=1)
		{

			if(pow(dx+col*i/(X_PRECISION*2),2.0)+pow(dy+row*j/(Y_PRECISION*2),2.0)<=scatRadius*scatRadius)
			{
				sum+=1.0;
			}

		}
	}

	/*
	incircle=X_PRECISION*Y_PRECISION*4;
	 */

	sum/=(Y_PRECISION*Y_PRECISION*4);
	return SCATTERER_EPSILON*sum+EPSILON_0*(1-sum);
}
