/*
 * constants.h
 *
 *  Created on: 2014/10/12
 *      Author: cplusc
 */

#ifndef CONSTANTS_H_
#define CONSTANTS_H_


#define M_PI 3.14159265358979323846264338327

#define C_0 0.7 //光速

#define EPSILON_0 1.0 //真空誘電率

#define MU_0 (EPSILON_0/C_0/C_0) //真空透磁率

#define SCATTERER_EPSILON (EPSILON_0*1.6*1.6)

#endif /* CONSTANTS_H_ */
