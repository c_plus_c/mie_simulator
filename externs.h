/*
 * externs.h
 *
 *  Created on: 2014/10/12
 *      Author: cplusc
 */

#ifndef EXTERNS_H_
#define EXTERNS_H_

#include "constants.h"
#include <complex.h>
#include <GL/glut.h>

//実世界上のサイズ
extern int n_x;
extern int n_y;

//PMLのセル数
extern int n_pml;

//セル数
extern int n_px;
extern int n_py;

//合計セル数
extern int n_cell;

//グリッド分割数(256分割)
extern int gridNum;

//グリッド間隔
extern double h_u;

//光速
extern double v_s;

//各周波数
extern double omega;

extern double lambda;

extern double waveAngle;

extern double waveCoefX;

extern double waveCoefY;

//波数パラメータ
extern double k;

//カウンタ
extern int time;

extern double u_ns;

extern double scatterer_epsilon;

//描画用データバッファ
extern GLubyte *draw_buffer;

extern double magnetic_permeability;


#endif /* EXTERNS_H_ */
