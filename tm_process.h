/*
 * tm_process.h
 *
 *  Created on: 2014/10/12
 *      Author: cplusc
 */

#ifndef TM_PROCESS_H_
#define TM_PROCESS_H_

#include "externs.h"

void tm_init();

void tm_update();

void tm_setdraw();

void tm_end();

#endif /* TM_PROCESS_H_ */
