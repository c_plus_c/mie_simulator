/*
 * simulator.h
 *
 *  Created on: 2014/10/12
 *      Author: cplusc
 */

#ifndef SIMULATOR_H_
#define SIMULATOR_H

#include "externs.h"
#include <math.h>
#include <complex.h>

enum CalcMode
{
	TM_2D,
	TE_2D
};

void simulator_setup(enum CalcMode mode);

void simulator_init(int _width,int _height, int _h, int _pml, double _lambda, double _waveAng, int _maxStep);

void simulator_update();

void simulator_setdraw();

void simulator_end();

/*
double calc_u_dash(double permittivity,double conductivity);
*/

//1次元配列に変換
int ind(int i, int j);

double complex cbilinear(double complex *p, double x, double y, int width, int height);

enum CondactivityMode
{
	ELECTRIC,
	MAGNETIC
};
double conductivityX(double x,enum CondactivityMode condactivityMode);
double conductivityY(double y,enum CondactivityMode condactivityMode);

double complex getWave(double x,double y,double amplitude,double angle,double time);
#endif /* SIMULATOR_H_ */
