#include "simulator.h"
#include "tm_process.h"
#include "te_process.h"
#include <math.h>
#include <GL/glut.h>
#include <stdio.h>

#define M 2
#define R (10e-8)

int ind(int i, int j)
{
  // return i*N_PY + j;
  return i + j*n_px;
}

double complex cbilinear(double complex *p, double x, double y, int width, int height)
{
	int i = floor(x);
	int j = floor(y);
	double dx = x - i;
	double dy = y - j;
	int index = i + j*width;
	return p[index]*(1.0-dx)*(1.0-dy)
			+ p[index+1]*dx*(1.0-dy)
			+ p[index+height]*(1.0-dx)*dy
			+ p[index+height+1]*dx*dy;
}

double complex getWave(double x,double y,double amplitude,double angle,double time)
{
	double xy = x*cos(angle)+y*sin(angle);

	return cexp((k*xy - omega*time)*I);
}

//実世界上のサイズ
int n_x;
int n_y;

//PMLのセル数
int n_pml;

//セル数
int n_px;
int n_py;

//合計セル数
int n_cell;

//グリッド分割数(256分割)
int gridNum;

//グリッド間隔
double h_u;

//光速
double v_s;

//各周波数
double omega;

//波数パラメータ
double k;

//カウンタ
int time;

//波長
double lambda;

double waveAngle;

//透磁率
double magnetic_permeability;

GLubyte *draw_buffer=NULL;

enum CalcMode currentMode;

double sigma_max;

double u_ns;

double waveCoefX;

double waveCoefY;

void (*init)();

void (*update)();

void (*setdraw)();

void (*end)();

void simulator_setup(enum CalcMode mode)
{
	switch(mode)
	{
	case TM_2D:
		init = tm_init;
		update = tm_update;
		setdraw = tm_setdraw;
		end = tm_end;
		break;
	case TE_2D:
		init = te_init;
		update = te_update;
		setdraw = te_setdraw;
		end = te_end;
		break;
	}
}

void simulator_init(int _width,int _height, int _h, int _pml, double _lambda, double _waveAng, int _maxStep)
{
	h_u = _h;

	n_x = _width/h_u;
	n_y = _height/h_u;

	n_pml = _pml;

	n_px = n_x + 2*n_pml;
	n_py = n_y + 2*n_pml;

	n_cell = n_px * n_py;

	waveAngle = _waveAng;

	waveCoefX= sin(waveAngle/180.0*M_PI);
	waveCoefY= cos(waveAngle/180.0*M_PI);

	//init_cylinder_scatterer();

	v_s = C_0;
	lambda = _lambda / h_u;
	k = 2 * M_PI / lambda;
	omega = k*v_s;

	draw_buffer=(GLubyte*)malloc(sizeof(GLubyte)*n_cell*3);

	magnetic_permeability = 1 / v_s / v_s;

	sigma_max = -((M+1)*EPSILON_0*v_s)/(2*n_pml)*log(R);

	u_ns = sin(omega/2)/sin(k/2);
	time=0;
	init();
}

void simulator_update()
{
	update();

	++time;
}


void simulator_setdraw()
{
	setdraw();
	//テクスチャの内容を更新
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, n_px, n_py, 0,
		GL_RGB, GL_UNSIGNED_BYTE, draw_buffer);

	//テクスチャを貼り付けた四角形を描画
	glClear(GL_COLOR_BUFFER_BIT);
	glBegin(GL_POLYGON);

	glTexCoord2f(0, 0); glVertex2f(-1, -1);
	glTexCoord2f(0, 1); glVertex2f(-1, 1);
	glTexCoord2f(1, 1); glVertex2f(1, 1);
	glTexCoord2f(1, 0); glVertex2f(1, -1);

	glEnd();

	//コマンドバッファの消化
	glFlush();

	//画面を更新
	glutPostRedisplay();
}

void simulator_end()
{
	end();
	free(draw_buffer);
}

/*
double calc_u_dash(double permittivity,double conductivity)
{
	double alpha = conductivity / (2 * permittivity);
	double sin2_1 = pow(sin((sqrt(omega*omega - alpha*alpha)) / 2), 2);
	double sinh2_2 = pow(sinh(alpha / 2), 2);
	double sin2_2 = pow(sin(k / 2), 2);
	double cosh_1 = cosh(alpha);

	return sqrt((sin2_1 + sinh2_2) / (sin2_2*cosh_1));
}
*/

double conductivityX(double x,enum CondactivityMode condactivityMode)
{
	if(x>=n_pml&&x<=n_px-n_pml)
		return 0;

	double retValue = 0;
	switch(condactivityMode)
	{
	case ELECTRIC:
		retValue = sigma_max * ((x<n_pml)?pow((n_pml-x)/n_pml,M):pow((x-(n_px - n_pml -1))/n_pml,M));
		break;
	case MAGNETIC:
		retValue = sigma_max * ((x<n_pml)?pow((n_pml-x)/n_pml,M):pow((x-(n_px - n_pml -1))/n_pml,M));
		retValue*=(MU_0/EPSILON_0);
		break;
	}

	return retValue;
}

double conductivityY(double y,enum CondactivityMode condactivityMode)
{
	if(y>=n_pml&&y<=n_py-n_pml)
		return 0;

	double retValue = 0;
	switch(condactivityMode)
	{
	case ELECTRIC:
		retValue = sigma_max * ((y<n_pml)?pow((n_pml-y)/n_pml,M):pow((y-(n_py - n_pml -1))/n_pml,M));
		break;
	case MAGNETIC:
		retValue = sigma_max * ((y<n_pml)?pow((n_pml-y)/n_pml,M):pow((y-(n_py - n_pml -1))/n_pml,M));
		retValue*=(MU_0/EPSILON_0);
		break;
	}

	return retValue;
}
