#include <GL/glut.h>
#include <stdio.h>
#include "externs.h"
#include "simulator.h"

void display(void)
{
	simulator_setdraw();
}

void idle(void)
{
	simulator_update();
}

int main(int argc, char *argv[])
{

	printf("Please select mode. TM:1, TE:2\n");

	fflush(stdout);
	int mod=1;
	scanf("%d",&mod);

	if(mod==1)
	{
		simulator_setup(TM_2D);
	}
	else
	{
		simulator_setup(TE_2D);
	}
	simulator_init(2560,2560, 10, 10, 500, 0, 99999999);
	//OpenGLの初期化処理一覧
	glutInit(&argc, argv);
	glutInitWindowSize(n_px, n_py);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA | GLUT_DEPTH);
	glutCreateWindow(argv[0]);
	glutIdleFunc(idle);
	glutDisplayFunc(display);

	//テクスチャ
	GLuint textures;

	//テクスチャに番号を割り当てる（識別子）
	glGenTextures(1, &textures);

	//textures変数のテクスチャ種類を2Dに束縛
	glBindTexture(GL_TEXTURE_2D, textures);

	//テクスチャのスケーリング時の補間方法を指定
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	//2Dテクスチャ機能を有効化
	glEnable(GL_TEXTURE_2D);

	//メインループ
	glutMainLoop();

	//テクスチャを開放する
	glDeleteTextures(1, &textures);

	simulator_end();
	return 0;
}
