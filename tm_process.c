#include "tm_process.h"
#include "simulator.h"
#include "cylinder_scatterer.h"
#include <stdio.h>
#include <math.h>

//誘電率
static double *EpsEz=NULL;
static double *EpsHx=NULL;
static double *EpsHy=NULL;

//Hx(x,y)
static double complex *Hx=NULL;

//Hy(x,y)
static double complex *Hy=NULL;

//Ez(x,y)
static double complex *Ezx=NULL;
static double complex *Ezy=NULL;
static double complex *Ez=NULL;

static double complex *JEx = NULL;
static double complex *JEy = NULL;
static double complex *JE = NULL;

static double *C_HX1=NULL;
static double *C_HX2=NULL;

static double *C_HY1=NULL;
static double *C_HY2=NULL;

static double *C_EZX1=NULL;
static double *C_EZX2=NULL;

static double *C_EZY1=NULL;
static double *C_EZY2=NULL;

static double gamma2;

static void addWave()
{
	double rad = waveAngle;
	double ray_coef = 1.0 - exp(-0.0001*time*time);
	//電場を印加
	for (int i = n_pml; i < n_px - n_pml; ++i)
	{
		for (int j = n_pml; j < n_py - n_pml; ++j)
		{
			double kr = 1.0*i*k*cos(rad) + 1.0*j*k*sin(rad);
			double n = sqrt( EpsEz[ind(i,j)] / EPSILON_0);
			double u0 = sin(omega*0.5) / sin(k*0.5);
			double u1 = sin(omega*0.5) / sin(k*n*0.5);
			double _n = u0 / u1;

			double complex w = ray_coef * (1.0/(_n*n) - 1.0)* (cexp(I*(kr-omega*(time+1))) - cexp(I*(kr-omega*(time-0))));
			Ezy[ind(i,j)] += w*waveCoefY;
			Ezx[ind(i,j)] += w*waveCoefX;
		}
	}
}

static double getUnsHx(int x,int y)
{
	double n = sqrt(EpsHx[ind(x,y)]/EPSILON_0);
	return sin(omega/2)/sin(n*k/2);
}

static double getUnsHy(double x,double y)
{
	double n = sqrt(EpsHy[ind(x,y)]/EPSILON_0);
	return sin(omega/2)/sin(n*k/2);
}

static double getUnsEz(double x,double y)
{
	double n = sqrt(EpsEz[ind(x,y)]/EPSILON_0);
	return sin(omega/2)/sin(n*k/2);
}

void tm_init()
{
	printf("TM Init\n");

	Hx = (double complex*)malloc(sizeof(double complex)*n_cell);
	Hy = (double complex*)malloc(sizeof(double complex)*n_cell);
	Ezx = (double complex*)malloc(sizeof(double complex)*n_cell);
	Ezy = (double complex*)malloc(sizeof(double complex)*n_cell);
	Ez = (double complex*)malloc(sizeof(double complex)*n_cell);

	JEx = (double complex*)malloc(sizeof(double complex)*n_cell);
	JEy = (double complex*)malloc(sizeof(double complex)*n_cell);
	JE = (double complex*)malloc(sizeof(double complex)*n_cell);

	EpsHx = (double*)malloc(sizeof(double)*n_cell);
	EpsHy = (double*)malloc(sizeof(double)*n_cell);
	EpsEz = (double*)malloc(sizeof(double)*n_cell);

	C_HX1 = (double*)malloc(sizeof(double)*n_cell);
	C_HX2 = (double*)malloc(sizeof(double)*n_cell);

	C_HY1 = (double*)malloc(sizeof(double)*n_cell);
	C_HY2 = (double*)malloc(sizeof(double)*n_cell);

	C_EZX1 = (double*)malloc(sizeof(double)*n_cell);
	C_EZX2 = (double*)malloc(sizeof(double)*n_cell);

	C_EZY1 = (double*)malloc(sizeof(double)*n_cell);
	C_EZY2 = (double*)malloc(sizeof(double)*n_cell);


	//電磁界初期化
	for (int i = 0; i < n_px; ++i)
	{
		for (int j = 0; j < n_py; ++j)
		{
			Hx[ind(i,j)] = 0;
			Hy[ind(i,j)] = 0;
			Ezx[ind(i,j)] = 0;
			Ezy[ind(i,j)] = 0;
			Ez[ind(i,j)] = 0;

			JEx[ind(i,j)] = 0;
			JEy[ind(i,j)] = 0;
			JE[ind(i,j)] = 0;

			C_HX1[ind(i,j)] = C_HY1[ind(i,j)] = C_EZX1[ind(i,j)] = C_EZX2[ind(i,j)] = 1;
			C_HX2[ind(i,j)] = C_HY2[ind(i,j)] = C_EZY1[ind(i,j)]= C_EZY2[ind(i,j)] = 1;
		}
	}

	//誘電率初期化
	for(int i=0;i<n_px;++i)
	{
		for(int j=0;j<n_py;++j)
		{
			EpsHx[ind(i,j)] = get_epsilon(i,j+0.5,1,1,n_px/2, n_py/2, lambda,0,1);
			EpsHy[ind(i,j)] = get_epsilon(i+0.5,j,1,1,n_px/2, n_py/2, lambda,1,0);
			EpsEz[ind(i,j)] = get_epsilon(i,j,1,1,n_px/2, n_py/2, lambda,1,1);
		}
	}

	for (int i = 0; i < n_px; ++i)
	{
		for (int j = 0; j < n_py; ++j)
		{
			double alpha_s = conductivityY(j+0.5,ELECTRIC)/(2*EpsHx[ind(i,j)]);
			double alpha = conductivityY(j+0.5,MAGNETIC)/(2*magnetic_permeability);
			double beta = tanh(alpha)/(1+tanh(alpha)*tanh(alpha_s));
			double betaplus = 1 + beta;
			double betaminus = 1 - beta;
			double udash = getUnsHx(i,j);
			double coef = udash*sqrt(EpsHx[ind(i,j)] / magnetic_permeability);
			C_HX1[ind(i,j)] = betaminus/betaplus;
			C_HX2[ind(i,j)] = coef / betaplus;
		}
	}

	for (int i = 0; i < n_px; ++i)
	{
		for (int j = 0; j < n_py; ++j)
		{
			double alpha_s = conductivityX(i+0.5,ELECTRIC)/(2*EpsHy[ind(i,j)]);
			double alpha = conductivityX(i+0.5,MAGNETIC)/(2*magnetic_permeability);
			double beta = tanh(alpha)/(1+tanh(alpha)*tanh(alpha_s));
			double betaplus = 1 + beta;
			double betaminus = 1 - beta;
			double udash = getUnsHy(i,j);
			double coef = udash*sqrt(EpsHy[ind(i,j)] / magnetic_permeability);
			C_HY1[ind(i,j)] = betaminus/betaplus;
			C_HY2[ind(i,j)] = coef / betaplus;
		}
	}

	//γ'の計算 式(3.4.26)
	{
		gamma2 = 1.0/12.0;// + k*k/180.0 - pow(k,4)/23040.0)/2.0;
	}

	for (int i = 0; i < n_px; ++i)
	{
		for (int j = 0; j < n_py; ++j)
		{
			double alpha_s = conductivityX(i,ELECTRIC)/(2*EpsEz[ind(i,j)]);
			double alpha = conductivityX(i,MAGNETIC)/(2*magnetic_permeability);
			double beta = tanh(alpha_s)/(1+tanh(alpha)*tanh(alpha_s));
			double betaplus = 1 + beta;
			double betaminus = 1 - beta;
			double udash = getUnsEz(i,j);
			double coef = udash * sqrt(magnetic_permeability / EpsEz[ind(i,j)]);
			C_EZX1[ind(i,j)] = betaminus/betaplus;
			C_EZX2[ind(i,j)] = coef / betaplus;
			//C_EZX3[ind(i,j)] = 1 / betaplus;
		}
	}

	for (int i = 0; i < n_px; ++i)
	{
		for (int j = 0; j < n_py; ++j)
		{
			double alpha_s = conductivityY(j,ELECTRIC)/(2*EpsEz[ind(i,j)]);
			double alpha = conductivityY(j,MAGNETIC)/(2*magnetic_permeability);
			double beta = tanh(alpha_s)/(1+tanh(alpha)*tanh(alpha_s));
			double betaplus = 1 + beta;
			double betaminus = 1 - beta;
			double udash = getUnsEz(i,j);
			double coef = udash * sqrt(magnetic_permeability / EpsEz[ind(i,j)]);
			C_EZY1[ind(i,j)] = betaminus/betaplus;
			C_EZY2[ind(i,j)] = coef/betaplus;
			//C_EZY3[ind(i,j)] = 1/betaplus;
		}
	}
}

void tm_update()
{

	/*
	hx[x][y]=Hx(x,y+1/2)
	hy[x][y]=Hy(x+1/2,y)
	ez[x][y]=Ez(x,y)
	として定義する。
	 */

	//Hxの計算 式(3.4.31)
	for (int i = 1; i < n_px-1; ++i)
	{
		for (int j = 0; j < n_py-1; ++j)
		{
			double complex d1 = (Ez[ind(i,j+1)]-Ez[ind(i,j)]);
			double complex d2 = ((Ez[ind(i+1,j+1)]-Ez[ind(i+1,j)]) + (Ez[ind(i-1,j+1)]-Ez[ind(i-1,j)]) -2*(d1))*gamma2;
			double complex diff=d1+d2;

			Hx[ind(i,j)] = C_HX1[ind(i,j)]*Hx[ind(i,j)] - C_HX2[ind(i,j)] *diff;
		}
	}

	//Hyの計算 式(3.4.31)
	for (int i = 0; i < n_px-1; ++i)
	{
		for (int j = 1; j < n_py-1; ++j)
		{

			double complex d1 = (Ez[ind(i+1,j)]-Ez[ind(i,j)]);
			double complex d2 = ((Ez[ind(i+1,j+1)]-Ez[ind(i,j+1)]) + (Ez[ind(i+1,j-1)]-Ez[ind(i,j-1)]) -2*(d1))*gamma2;
			double complex diff=d1+d2;

			Hy[ind(i,j)] = C_HY1[ind(i,j)]*Hy[ind(i,j)] + C_HY2[ind(i,j)] *diff;
		}
	}

	//Ezxの計算
	for (int i = 1; i < n_px; ++i)
	{
		for (int j = 0; j < n_py; ++j)
		{
			Ezx[ind(i,j)] = C_EZX1[ind(i,j)]*Ezx[ind(i,j)] + C_EZX2[ind(i,j)] *(Hy[ind(i,j)]-Hy[ind(i-1,j)]);
		}
	}

	//Ezyの計算
	for (int i = 0; i < n_px; ++i)
	{
		for (int j = 1; j < n_py; ++j)
		{

			Ezy[ind(i,j)] = C_EZY1[ind(i,j)]*Ezy[ind(i,j)] - C_EZY2[ind(i,j)] *(Hx[ind(i,j)]-Hx[ind(i,j-1)]);
		}
	}

	addWave();

	for (int i = 0; i < n_px; ++i)
	{
		for (int j = 0; j < n_py; ++j)
		{
			Ez[ind(i,j)] = Ezx[ind(i,j)]+Ezy[ind(i,j)];
		}
	}

	if(time==2000)
	{
		printf("CSV Saved");
		FILE *fp = fopen("tm_out.csv","w");
		for(int i=180;i>=0;--i)
		{
			double complex val = cbilinear(Ez, n_px/2.0 + lambda*1.2*cos(i*M_PI/180.0), n_py/2.0 + lambda*1.2*sin(i*M_PI/180.0), n_px, n_py);
			fprintf(fp,"%f\n",cabs(val));
		}

		fclose(fp);
	}
}


void tm_setdraw()
{
	//描画データ更新


	for (int i = 0; i < n_px; ++i)
	{
		for (int j = 0; j < n_py; ++j)
		{
			double ezr = cabs(Ez[ind(i,j)]);

			double range = 1.0; //波の振幅
			double ab_phi = ezr < 0 ? -ezr : ezr;

			double a = ab_phi < range ? (ab_phi <  range/3.0 ? 3.0/range*ab_phi : (-3.0/4.0/range*ab_phi+1.25) ) : 0.5;

			double r = ezr > 0 ? a:0;
			double b = ezr < 0 ? a:0;
			double g = fmin(1.0, fmax(0.0, -3*ab_phi+2));

			draw_buffer[ind(i,j)*3]   = (GLubyte)( fmax(0, 255*(r - 0.5*(EpsEz[ind(i,j)] - EPSILON_0))));
			draw_buffer[ind(i,j)*3+2] = (GLubyte)( fmax(0, 255*(b - 0.5*(EpsEz[ind(i,j)] - EPSILON_0))));
			draw_buffer[ind(i,j)*3+1] = (GLubyte)( fmax(0, 255*(g - 0.5*(EpsEz[ind(i,j)] - EPSILON_0))));
		}
	}

	//printf("%f\n",Ez[ind(52,50)]);
	fflush(stdout);
}

void tm_end()
{
	printf("TM End\n");
	free(Hx);
	free(Hy);
	free(Ez);
	free(Ezx);
	free(Ezy);

	free(EpsHx);
	free(EpsHy);
	free(EpsEz);

	free(C_HX1);
	free(C_HX2);

	free(C_HY1);
	free(C_HY2);

	free(C_EZX1);
	free(C_EZY1);
	free(C_EZX2);
	free(C_EZY2);

	free(JEx);
	free(JEy);
	free(JE);
}
