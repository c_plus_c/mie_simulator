/*
 * cylinderScatterer.h
 *
 *  Created on: 2014/10/12
 *      Author: cplusc
 */

#ifndef CYLINDER_SCATTERER_H_
#define CYLINDER_SCATTERER_H_

#include "externs.h"

double get_epsilon(double x, double y, double sizeX, double sizeY, double scatCenterX, double scatCenterY, double scatRadius,double col,double row);

#endif /* CYLINDER_SCATTERER_H_ */
